#include "CA_Cert.h"
#include "influx.h" //influx details (url/org/bucket/token)
#include "wifi.h"   //Wifi credentials
#include <Arduino.h>
#include <HTTPClient.h>
#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>
#include <OOKwiz.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <WiFiMulti.h>
#ifdef ARDUINO_M5Stack_ATOM
#include <M5Atom.h>
#endif

#define MAX_BATCH_SIZE 5
#define WRITE_BUFFER_SIZE 30
#define WRITE_PRECISION WritePrecision::S

#define TZ_INFO "PST8PDT"
// For the fastest time sync find NTP servers in your area:
// https://www.pool.ntp.org/zone/
#define NTP_SERVER1 "time.oregonstate.edu"
#define NTP_SERVER2 "pool.ntp.org"

WiFiMulti wifiMulti;

InfluxDBClient client(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET,
                      INFLUXDB_TOKEN, InfluxDbCloud2CACert);
Point megacode("Megacode-Remote");

long previousMillis = 0;
long interval = 60 * 1000;
// Number for loops to sync time using NTP
int iterations = 0;

bool connect() {
  // Connect WiFi
  Serial.println("Connecting to WiFi");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(100);
  }
  Serial.println(" CONNECTED");
  return true;
}

void receive(RawTimings raw, Pulsetrain train, Meaning meaning) {
  if (raw.intervals.size() != 47) { // 24 pulses + 23 gaps
    /*
    Serial.printf("Ignore %d intervals, expected 47 intervals\n",
                  raw.intervals.size());
    Serial.println(raw.toString());
    Serial.println(raw.visualizer());
    */
    return;
  }
  M5.dis.drawpix(0, 0xFF0000);
  unsigned long data = 1;
  uint8_t prev = data;
  unsigned long facility = 0;
  unsigned long remote = 0;
  unsigned long button = 0;
  for (size_t i = 0; i < raw.intervals.size(); i++) {
    if (i % 2 == 0) {
      // Ignore pulses, we measure gaps
      continue;
    }
    /*
      0 -> 1: > 6000
      1 -> 0: < 4000
      1 -> 1: ~ 5000
      0 -> 0: ~ 5000
    */
    if (raw.intervals[i] > 6000) {
      data = (data << 1) | 1;
      prev = 1;
    } else if (raw.intervals[i] < 4000) {
      data = (data << 1) | 0;
      prev = 0;
    } else {
      data = (data << 1) | prev;
    }
  }
  button = (data >> 0) & 0x07;
  remote = (data >> 3) & 0xFFFF;
  facility = (data >> 19) & 0x0F;
  if (facility == 0 || remote == 0 || button == 0) {
    Serial.printf("Invalid data: %lx\n", data);
    return;
  }
  Serial.printf("Facility: %d, Remote: %d, Button: %d\n", facility, remote,
                button);

  reportToInflux(data, facility, remote, button);
  M5.dis.drawpix(0, 0x00FF00);
}

void setup() {
#ifdef ARDUINO_M5Stack_ATOM
  M5.begin(true, false, true);
#endif
  M5.dis.drawpix(0, 0xFFFFFF);

  connect();

  timeSync(TZ_INFO, NTP_SERVER1, NTP_SERVER2);
  configTzTime(TZ_INFO, NTP_SERVER1, NTP_SERVER2);

  // Check server connection
  if (client.validateConnection()) {
    Serial.print("Connected to InfluxDB: ");
    Serial.println(client.getServerUrl());
  } else {
    Serial.print("InfluxDB connection failed: ");
    Serial.println(client.getLastErrorMessage());
  }

  client.setWriteOptions(WriteOptions()
                             .writePrecision(WRITE_PRECISION)
                             .batchSize(MAX_BATCH_SIZE)
                             .bufferSize(WRITE_BUFFER_SIZE));

  // set pin_cs 5;set pin_miso 19;set pin_mosi 23;set pin_rx 2;set pin_sck
  // 18;set radio CC1101;save;reboot
  Settings::set("radio", "CC1101");
  Settings::set("pin_cs", "19");
  Settings::set("pin_miso", "23");
  Settings::set("pin_mosi", "33");
  Settings::set("pin_rx", "21");
  Settings::set("pin_sck", "22");
  // set frequency 318.0;set bandwidth 650.0;set bitrate 0.5;
  Settings::set("frequency", "318.0");
  Settings::set("bandwidth", "650.0");
  Settings::set("bitrate", "0.5");
  // set first_pulse_min_len 1000; set pulse_gap_len_new_packet 8500;set
  // max_nr_pulses 50; set pulse_gap_min_len 500; set bin_width 1000;
  Settings::set("first_pulse_min_len", "950");
  Settings::set("pulse_gap_len_new_packet", "8500");
  Settings::set("min_nr_pulses", "23");
  Settings::set("max_nr_pulses", "25");
  Settings::set("pulse_gap_min_len", "750");
  Settings::set("bin_width", "1000");

  // Not sure about this
  Settings::set("rx_active_high", "true");

  // "Production" settings
  Settings::unset("print_raw");
  Settings::unset("print_visualizer");
  Settings::unset("print_summary");
  Settings::unset("print_pulsetrain");
  Settings::unset("print_binlist");
  Settings::unset("print_meaning");
  Settings::set("serial_cli_disable");

  OOKwiz::setup(true);
  OOKwiz::onReceive(receive);
  M5.dis.drawpix(0, 0x00FF00);
}

void loop() {
  OOKwiz::loop();
  /*
  M5.update();
  if (M5.Btn.isPressed()) { // If the button is pressed
    Serial.println("Button is pressed.");
    connect();
  }

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
 }
  */
}

void reportToInflux(uint32_t raw, uint8_t facility, uint16_t remote, uint8_t button) {
  // If no Wifi signal, try to reconnect it
  if ((WiFi.RSSI() == 0) && (wifiMulti.run() != WL_CONNECTED)) {
    Serial.println("Wifi connection lost");
    connect();
  }

  megacode.clearFields();
  megacode.clearTags();
  megacode.setTime(time(nullptr));
  megacode.addTag("device", "M5Atom");

  megacode.addTag("id", String(remote));
  megacode.addField("facility", facility);
  megacode.addField("button", button);
  Serial.println(client.pointToLineProtocol(megacode));

  if (!client.writePoint(megacode)) {
    Serial.print("InfluxDB write failed: ");
    Serial.println(client.getLastErrorMessage());
  }

  // End of the iteration - force write of all the values into InfluxDB as
  // single transaction
  Serial.println("Flushing data into InfluxDB");
  if (!client.flushBuffer()) {
    Serial.print("InfluxDB flush failed: ");
    Serial.println(client.getLastErrorMessage());
    Serial.print("Full buffer: ");
    Serial.println(client.isBufferFull() ? "Yes" : "No");
    client.resetBuffer();
  }
}
